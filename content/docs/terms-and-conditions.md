---
title: "Terms and Conditions" 
weight: 10
alwaysopen: false
collapsibleMenu: "false"
---

![image](/images/t-and-c.png)

### [T&C.pdf](https://static.alta3.com/pdf/terms-and-conditions.pdf)

### Alta3 Research – Instructor-Led Training Terms and Conditions

#### Course Registration and Order Process
Customer must complete the applicable order form via email or the order page on Alta3 Research’s webterms-and-conditionssite to complete registration in an instructor-led open enrollment course. Student(s) will receive detailed connection instructions five (5) business days prior to the start of the course.
Orders for private course deliveries must contact sales@alta3.com for order processing.


#### Schedule and Communication
Alta3 Research’s course offerings, and the dates in which they are scheduled, are listed on alta3.com’s individual course pages and are modified several times a year. Alta3 Research reserves the right to change the schedule, add or remove courses, and modify the course prices at any time without previous notice. Generally, courses are scheduled on weekdays between 10:00 a.m. and 6:00 p.m. ET with a lunch break of approximately one hour.
By attending a course delivered by Alta3 Research, Customer consents to receive follow up communication related to Customer’s previously expressed interests. Customer may opt out at any time.

### Course Materials and Intellectual Property
An Alta3 Research “Student” is defined as a single user license. Student(s) are required to provide an active, single user email address, first name, and last name for account creation. The email address attached to the single user license must be specifically associated solely with the individual Student. Multiple Students may be enrolled in a course, and each shall be individually assigned a unique license. Each Student will be provided a personal, non-transferable license to access the course. During class, Student(s) will receive an individualized activation link (via their provided email address) to activate their account in Alta3 Research’s Learning Management System (“Alta3 Live”). Alta3 Research’s LMS content (videos, labs, exams, etc.) hereby known as “Training Material,” will be accessible to Student(s) for 1 year starting on the date of Student account creation.
Transferring or sharing their license with anyone else is a violation of the licensing agreement and is strictly prohibited. Sharing a license will result in the revocation of the license with no refund. License holders may not copy, reproduce, reverse engineer, translate, port, modify, or make derivative works of the Training Material. Student(s) shall not derive or attempt to derive the source code, source files, or structure of any portion of the Training Material by reverse engineering, disassembly, decompilation, or any other means. Alta3 Research retains all exclusive ownership rights, title and interests in the Training Material, including, but not limited to ownership rights in all copyrights, trademarks, service marks, or trade secrets. The Training Material is copyrighted and may not be copied, distributed, or reproduced in any form, in whole or in part even if modified or merged with other Training Material. Student shall not alter or remove any copyright notice or proprietary notice contained in or on the Products. Student shall take all reasonable steps and precautions to ensure that the use of the Training Material is in strict compliance with this License Agreement.
Once any applicable subscription period has concluded, the license granted shall immediately terminate and Student(s) have no further right to access, review or use in any manner any Alta3 Research Training Material. Alta3 Research reserves the right to terminate a subscription if, at its sole discretion, it believes Student(s) are in violation of this License Agreement or if Alta3 Research believes Student(s) have exceeded reasonable usage. Upon the occurrence of any of these termination events, no refund will be made of any amounts previously paid to Alta3 Research.
Online and onsite courses prohibit recording the course in any manner. This includes the use of video capture software or any other audio and/or video recording of the course session. The course material taught in class must not be recorded or rebroadcasted in any capacity as this violates the user license agreement and intellectual property policies.

### Payment Terms

Payment must be received at time of registration or a valid PO must be provided. Payment made via invoice must be paid within invoice terms.
Alta3 Research accepts payment by ACH transfer, check, or credit card only.
All travel and other expenses are the responsibility of Customer. Alta3 Research is not responsible for travel or other expenses incurred by Customer.
For Services that require payment by credit card, Alta3 Research uses a third-party credit card processing service to process payments. Customer consents to the use of such service and to the transfer of Customer’s credit card details to such third-party processor.
Alta3 Research is unable to apply discount codes to orders that have already been placed.

### Cancellation and Reschedule Policies

A.    Open Enrollment (Public) Instructor-Led Courses
Alta3 Research reviews course enrollments for its open enrollment courses at least ten (10) business days prior to the course start date. Alta3 Research, in its discretion, may cancel or reschedule any scheduled course if certain minimum enrollment is not achieved, due to instructor illness, or due to other events outside of its control. If this occurs, Customer will be notified via email and may transfer their registration to a later class within twelve (12) months of their original course date.
Customers may request to reschedule attendance of a class via email to registrars@alta3.com at least ten (10) days prior to the start of the Course. If Customer requests to reschedule, the Course must be completed within twelve (12) months of the initial Course date. If Customer requests to reschedule between zero to nine (0-9) days prior to the start of the Course, fees paid or owed are forfeited and non-cancellable, though Customer may enroll in a future Guaranteed-To-Run (GTR) Course within six (6) months of the original training date. If no future GTR Course exists, any fees paid or owed are forfeited and non-cancellable.
In the event that customer does not use all of the Training Services within twelve (12) months after the date of purchase, the Training Services will expire without any refund and/or reimbursement due to Customer.
In the event of non-attendance or failure to reschedule in accordance with these Terms, any fees paid or owed are forfeited and non-cancellable.

B.   Private Instructor-Led Courses
If the Customer reschedules or cancels the training event 11-20 business days before the scheduled session date, 25% of the base delivery fee will be charged to the Customer. If the Customer reschedules or cancels the training event 10 business days or less from the scheduled session date, 50% of the base delivery fee will be charged to the Customer as well as any non-refundable expenses such as airfare. If payment has already been made to Alta3 Research, then the total amount paid minus the cancellation penalty will be refunded by Alta3 Research to the Customer via a check within thirty (30) days. 

### Student Connection Test Requirements

Digital copies of the course materials will be provided by the instructor during class. Students’ online course materials can be accessed with either the Google Chrome or Microsoft Edge browser. Students must test their connectivity to the lab environment by going to test.alta3.com. A successful connection will display a black screen with the "Live Connectivity Test Success" message.

### Onsite Training Requirements

Alta3 Research course materials are accessed through live.alta3.com. Therefore, a strong internet connection is required for successful course delivery. Alta3 Research requires symmetric access (a minimum of 100 Mbps download and upload speed) per student. Although not required, a back-up internet connection should be considered in case of outage or technical difficulties. 
The classroom must be equipped with a High-Definition projector (1920 x 1080 resolution or greater), large projection screen, extension cords, and power strips. At least one week prior to class, onsite lab connectivity must be tested to ensure network access to live.alta3.com. This step is critical for ensuring that corporate firewalls, VPNs, and other network configurations are configured to accommodate Alta3 Research’s training platform (full unrestricted internet accessibility is expected). If customer’s training facility network is strictly filtering internet sites, the following domains must be added to the Allow Lists for access and trusted certificates: live.alta3.com, live.prod.alta3.com, static.alta3.com, labs.alta3.com, *.live.alta3.com. Access to these sites must be thoroughly tested with Alta3 coordinators at least one week prior to class start. 
Test.alta3.com is available for connectivity and browser testing. A successful connection will display a black screen with the "Live Connectivity Test Success" message. Each student must be equipped with a computer in order to access the course materials and/or hands-on lab environment. The students’ computer must have the Google Chrome or the Microsoft Edge Web Browser installed. 
Students are responsible for their own travel costs and hotel accommodations.

### Privacy

[See Privacy Statement](docs/privacypolicy/)
