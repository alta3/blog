---
title: "HUGO found no layout file for HTML for kind home"
date: 2024-02-24
draft: false
headingPost: "Author: Stu Feeser"
menuPre: "<img src='/images/err2fix-logo.png' style='height:25px; vertical-align: middle;' /> "
---

### ERR:

`found no layout file for "HTML" for "section"`

### 2: Explanation

> If you are installing hugo with apt and testing is failing with the above error, then here is a potential cause, as spelled out here on a stackoverflow, where the answer given was correct but was downvoted: [solution (soft of)](https://stackoverflow.com/a/76439645).

> Here is my version of that downvoted solution for all you debian derivative users:

> IMPORTANT: REPLACE `hugo_extended_0.123.3_linux-amd64.deb` with today's latest. The following is an example to see what you must do:

### FIX:

`wget https://github.com/gohugoio/hugo/releases/download/v0.123.3/hugo_extended_0.123.3_linux-amd64.deb`

`sudo dpkg -i hugo_extended_0.123.3_linux-amd64.deb`

`rm hugo_extended_0.123.3_linux-amd64.deb`
```


