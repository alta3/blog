---
title: "Unlocking Your Vim Screen"
date: 2024-02-22
draft: false
headingPost: "Author: Sam Griffith"
tags:
- vim
---
We have all been there. We go to save our script in **vim**, then all of a sudden we have a terminal that has become unresponsive.

Trying to press <kbd>Esc</kbd> <kbd>:</kbd><kbd>q</kbd> <kbd>Enter</kbd> to exit fails. 

Trying to force exit by pressing <kbd>Esc</kbd> <kbd>:</kbd><kbd>q</kbd><kbd>!</kbd> <kbd>Enter</kbd> fails. 

Trying to press <kbd>i</kbd> to INSERT fails.

Then we remember, we were just trying to save our script. And what we did was what we have learned to do in any "sensical" program. 

**We hit the dreaded <kbd>Ctrl</kbd> <kbd>s</kbd>.**

This automatically freezes the screen for you. It must be helpful for somebody... just not you or I when using vim.

There is a purpose to it though. When you want to _stall_ the output of some command, hit <kbd>Ctrl</kbd> <kbd>s</kbd>. I've personally found this to be very useful for pausing the output of a `tail -f /var/log/syslog` command.

### The Fix

Thankfully, the fix for this is just as easy.

Just press <kbd>Ctrl</kbd> <kbd>q</kbd>, and you are back on your way to [writing excellent code](https://alta3.com/courses)!
