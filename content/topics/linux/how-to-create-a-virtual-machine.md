---
title: "How To Create A Virtual Machine"
date: 2024-02-22
draft: false
headingPost: "Author: Chad Feeser"
---
**[Alta3 Instructor](https://alta3.com/courses) Chad Feeser talks us through the process of installing Ubuntu on a virtual machine (vm).**

<iframe width="840" height="427" src="https://www.youtube.com/embed/S0muu92ko00?si=u2-lPVqWHUdkOlK6"  frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Come take [a course with Alta3](https://alta3.com/courses)!