---
title: "Getting Started With Vim"
date: 2024-02-22
draft: false
headingPost: "Author: Timothy Patrick"
tags:
- vim
---
[Alta3](https://alta3.com/courses) Instructor Tim Patrick provides an introduction to _vim_:

<iframe width="1120" height="630" src="https://www.youtube.com/embed/y7cWMV0jLWU?si=mEVoiMRn0hI3j9WC" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>