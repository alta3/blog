---
title: "Volume Mounts Meet MinIO Magic"
date: 2024-10-29
draft: false
headingPost: "Author: Stuart Feeser"
---

In the world of Docker, ensuring data persistence across container rebuilds and system-wide recoverability needs to be designed into a solution from the start. While **volume mounts** allow data persistence for containers, they have limitations when it comes to backup and recovery, especially if the system itself fails. Enter **MinIO**, an S3-compatible object storage solution that brings reliability and magic to your data.

By pairing Docker volume mounts with MinIO, you gain the best of both worlds: **data persistence** across container rebuilds and **disaster recovery** in case of full system rebuilds. This blog explores how to set up volume mounts with MinIO for an unbeatable storage solution.

## Why Volume Mounts Alone Aren’t Enough

Docker **volume mounts** are a great solution for persisting data across container lifecycles. By storing data outside the container filesystem in Docker-managed storage, volume mounts allow data to remain intact even if the container is stopped, restarted, or redeployed. However, this approach has limitations:

- **Host Dependency**: Volume mounts rely on the local Docker host’s storage. If the host goes down or you need to move to a new system, the volumes don’t transfer easily.
- **Lack of Off-Site Backup**: With only volume mounts, data is at risk if there is hardware failure, accidental deletion, or corruption on the host.

This is where MinIO comes into play. With MinIO, you can continuously back up your volume data to a separate, resilient storage solution that remains accessible no matter what happens to the host system.

## Enter MinIO: Making Volume Mounts Resilient

**MinIO** is an open-source, high-performance object storage system compatible with the S3 API. By regularly syncing your Docker volume data to MinIO, you create a reliable backup that can be easily restored, even if the original host is lost. This approach offers two main benefits:

1. **Persistence Over Rebuilds**: Volume data remains intact within MinIO backups, so you can recreate volumes with the same data on any new system.
2. **Disaster Recovery**: In case of a complete system failure, you can restore volumes from MinIO backups, rebuilding a Docker environment from scratch without data loss.

Let’s dive into a setup that brings this magic to life.

## Step-by-Step Guide: Pairing Volume Mounts with MinIO

### Step 1: Install MinIO

First, you’ll need to set up MinIO. You can install it on your local network, cloud infrastructure, or even use a managed MinIO service.

1. **Run MinIO in Docker**:
   ```bash
   docker run -p 9000:9000 -p 9001:9001 --name minio \
     -e "MINIO_ROOT_USER=minioadmin" \
     -e "MINIO_ROOT_PASSWORD=minioadminpassword" \
     minio/minio server /data --console-address ":9001"
   ```
2. **Access MinIO Console**: Visit `http://localhost:9001` and log in with the credentials provided above.

3. **Create a Bucket**: Inside the MinIO console, create a new bucket (e.g., `volume-backups`) where volume data will be stored.

### Step 2: Set Up the MinIO Client (`mc`) in the Container

Install the **MinIO client** (`mc`) within your container or Docker image, which will allow you to interact with MinIO storage directly from the container.

1. **Install MinIO Client**:
   ```bash
   curl -O https://dl.min.io/client/mc/release/linux-amd64/mc
   chmod +x mc
   sudo mv mc /usr/local/bin/
   ```
2. **Configure MinIO Client**:
   ```bash
   mc alias set my-minio http://minio-server:9000 minioadmin minioadminpassword
   ```

This alias allows you to refer to MinIO as `my-minio` in all `mc` commands.

### Step 3: Create a Backup Script in the Container

Create a script inside the container that periodically backs up volume data to MinIO.

1. **Backup Script Example**:
   ```bash
   #!/bin/bash
   TIMESTAMP=$(date +%F_%H-%M-%S)
   VOLUME_PATH="/data" # Path to the volume inside the container
   BUCKET="my-minio/volume-backups"

   # Sync the volume to MinIO
   mc cp -r $VOLUME_PATH "$BUCKET/$TIMESTAMP/"
   ```

2. **Save the Script**:
   Save this script as `backup.sh` inside the container and make it executable:
   ```bash
   chmod +x /path/to/backup.sh
   ```

### Step 4: Schedule the Backup Script

Set up a cron job or similar scheduled task to run this backup script regularly.

```bash
# Run backup script daily at midnight
0 0 * * * /path/to/backup.sh
```

### Step 5: Recovery: Restoring Volume Data from MinIO

If you need to restore data to a volume on a new system, MinIO makes it easy:

1. **Retrieve the Latest Backup**:
   List the MinIO bucket contents and locate the latest backup timestamp.
   ```bash
   mc ls my-minio/volume-backups
   ```

2. **Restore the Volume**:
   Use `mc` to copy the backup data to the appropriate volume path:
   ```bash
   mc cp -r my-minio/volume-backups/<latest_backup_timestamp> /data
   ```

3. **Restart Containers**:
   Start any containers that use the volume to ensure they recognize the restored data.

## Why MinIO + Volume Mounts Is the Perfect Combo

By using MinIO alongside Docker volume mounts, you achieve:

- **Robust Data Persistence**: Data remains accessible in MinIO regardless of container or host state.
- **Seamless Recovery**: MinIO’s S3-compatible storage allows for easy recovery, letting you restore volumes on any new system.
- **Automated Backups**: With MinIO, backups are off-host, automated, and readily available for recovery, making your volume data much safer.

