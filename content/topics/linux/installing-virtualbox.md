---
title: "Installing Virtualbox"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---
### Objective:
  1. Set up VirtualBox so that we can launch VMs for [Ansible](https://alta3.com/courses/ansible-101) to configure.
  2. Verify the download and install.

### Tasks:

1. Set up a symlink to our VirtualBox directory to make managing commands easier. We will map downloads -> /mnt/c/Users/\<username\>/Downloads/. Replace <username> with your user name, of course!

    `$` `cd ~/`

    `$` `ln -s  /mnt/c/Users/<username>/Downloads/ downloads`

0. Now we can easily cd into our Windows 10 downloads directory. 

    `$` `cd downloads/`

0. Create a `virtualbox` directory and `cd` into it.

    `$` `mkdir virtualbox && cd virtualbox`

0. **IMPORTANT**: Remain in this directory for all remaining steps.    

    `/mnt/c/Users/<YOURUSERNAME>/Downloads/virtualbox`

0. Download VirtualBox! Don't do this from the browser, do it here:

    `$` `wget https://download.virtualbox.org/virtualbox/6.1.18/VirtualBox-6.1.18-142142-Win.exe`

0. Now grab SHAMSUM256 from VirtualBox.org.

    `$` `wget https://www.virtualbox.org/download/hashes/6.1.18/SHA256SUMS`

0. Confirm there's nothing fishy about your version of VirtualBox.

    `$` `sha256sum --ignore-missing -c SHA256SUMS`

    ```
    HA256SUMS
    VirtualBox-6.1.18-142142-Win.exe: OK
    ```

0. Using File Explorer on your Windows 10 desktop, find the VirtualBox executable (VirtualBox-6.1.18-142142-Win.exe) and install VirtualBox.

0. That's it for this [lab](https://alta3.com/courses)!
