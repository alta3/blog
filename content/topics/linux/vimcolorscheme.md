---
title: "Setting a Vim color scheme"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
---
I was recently lecturing on [API and API Design with Python](https://alta3.com/courses/api) and was in a room where the projector was nearing its last days. With significantly less lumens, it was difficult to see the blue comments on black background. Perhaps your reasons are different, but if you're looking for instructions for setting a vim color scheme, read on.

From within vim, issue the following command to change to the 'morning' theme:

    ```bash
    :colorscheme morning
    ```

Now from within vim, issue the following command to change to the 'industry' theme:


    ```bash
    :colorscheme industry
    ```

If you're lazy, you can shorthand the colorscheme command as follows:

    ```bash
    :colo blue
    ```

To cycle through available choices, type the `:colorscheme` command, **space** then the **Tab** key. Continue to tap **Tab** to cycle through choices.

**Color Schemes Available in vim:**
- blue
- darkblue
- default
- delek
- desert
- elflord
- evening
- industry
- koehler
- morning
- murphy
- pablo
- peachpuff
- ron
- shine
- slate
- torte
- zellner  

All of these colors have corresponding files ending in `.vim`, for example, there is a `zellner.vim` file on your system that describes the zellner color profile.

On my Ubuntu machine, the location of these profiles is `/usr/share/vim/vim80/colors/` however on any [Linux system](https://alta3.com/courses/linux-4-dev) you should be able to locate the location of your profiles by running a command like the following:

`sudo find / -name zellner.vim`

Try displaying one of these `*.vim` profile files on the screen. Neat to see it once.

**Persistent color settings with `~/.vimrc`**

Setting a color scheme within a vim session is easy, but if you exit that session, your color preference will be lost. If you want your color choices to persist, place the settings within ~/.vimrc file. To edit this file, issue the following command:


`vim ~/.vimrc`

The `~/` means, "current user's home directory". The `.vimrc` is the name of the file. In Linux, any file preceded by a dot is hidden.



An example `~/.vimrc` file that would allow the elflord setting to persist would look like the following:

    ```
    colo elflord
    syntax on
    " Make comments in ~/.vimrc with preceding double-quotes
    ```

This is reposted from an [Alta3](https://alta3.com/courses) Instructor's personal blog. The original post may be viewed at <a rel="noreferrer noopener" aria-label="RZFeeser Setting vim Color Schemes (opens in a new tab)" href="https://www.rzfeeser.com/blog/setting-a-vim-color-scheme" target="_blank">RZFeeser Setting vim Color Schemes</a>.


**Helpful Links:**  

<ul><li><a rel="noreferrer noopener" aria-label="Vim Editor Homepage (opens in a new tab)" href="https://www.vim.org/" target="_blank">Vim Editor Homepage</a></li><li><a href="https://vim-adventures.com/" target="_blank" rel="noreferrer noopener" aria-label="Vim Adventure! (opens in a new tab)">Vim Adventure!</a></li></ul>
