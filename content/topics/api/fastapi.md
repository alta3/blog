---
title: "Creating a RESTful API"
date: 2024-02-22
draft: false
headingPost: "Author: Joe Hutcheson"
tags:
- api
- programming
- python
---
[FastAPI](https://fastapi.tiangolo.com/), a web framework for [developing RESTful APIs in Python](https://alta3.com/courses/api), has generated a fair amount of buzz since its inception in 2018. Microsoft, Uber and Netflix have all gone on record as having adopted the library to build out applications. FastAPI describes itself as "a modern, fast (high-performance), web framework for building APIs with Python 3.7+ based on standard Python type hints." Both in performance and development, the framework is appropriately named; FastAPI is one of the fastes Python frameworks available. In addition to speed, FastAPI boasts being developer friendly, intuitive, and is based on and compatible with current API standards. 

Continuing with the theme of speed, FastAPI automatically includes interactive API documentation with Swagger (alternatively ReDoc), further supporting its claims of speed by allowing developers to call and test their [APIs](https://alta3.com/courses/api) directly from the browser. Developers can simply append "/docs" to their base url to view the interactive Swagger documentation and "/redoc" for the ReDoc documentation. 

The fastapi PyPI entry may be found here [https://pypi.org/project/fastapi/](https://pypi.org/project/fastapi/) the install step is

```
pip install fastapi
```

It is no surprise that setup is quick and easy, but it does require an ASGI (Asynchronous Server Gateway Interface) such as [Uvicorn](https://www.uvicorn.org/), which will be used in this example. You may install uvicorn with the following

```
pip install "uvicorn[standard]"
```

The first code you'll need is something like this

```
#!/usr/bin/python3
"""
Quick Tutorial - FastAPI RESTful API Server by Alta3 Research
Demo a simple FastAPI server
"""

from fastapi import FastAPI
import uvicorn

```

We will need to create an instance of our FastAPI class. By convention, we generally assign our server object to the variable *app*:

`app = FastAPI()`

We define the endpoints or path operations of our URL with decorators referencing our server object:

`@app.get("/")`

We then define the path operation function below the decorator:

```
@app.get("/")
def hello_alta3():
    return "Hello Alta3 Research!"
```

While this example only addresses as GET request, we can accommodate the other HTTP verbs in the same manner, i.e. *.post()*, *.put()*, *.delete()*, etc. 

Finally, we run our server:

```
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=2224)
```

A completed script could look something like this:

```
#!/usr/bin/python3
"""
Quick Tutorial - FastAPI RESTful API Server by Alta3 Research
Demo a simple FastAPI server
"""

from fastapi import FastAPI
import uvicorn

app = FastAPI()

@app.get("/")
@app.get("/home")
def hello_alta3():
    return "Hello Alta3 Research!"
    
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=2224)
```

Instead of running our server programmatically (in our script), we can also call it from the command line. Used only in development, the --reload flag makes the server restart after code changes have been made, expediting the process. :

`uvicorn main:app --reload`

Once the script is up and running, we can add "/docs" to our base URL, which will render the following interactive Swagger documentation:

![image](https://labs.alta3.com/images/FastAPI_swagger.png)

If our application were also set up to accomodate POST/PUT/DELETE calls, we could test those out here. Appending "/redoc" to our base URL will yeild this:

![image](https://labs.alta3.com/images/FastAPI_redoc.png)

While ReDoc does not accommodate the same interactive testing experience, it does provide solid documentation for your API. 

To further put the "fast" in *FastAPI*, developers will find the documentation very easy to read and navigate. Even if you are just getting the hang of [working with or creating API's](https://alta3.com/courses/api), FastAPI can have you up and running in no time. 
