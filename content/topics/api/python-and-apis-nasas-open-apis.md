---
title: "Python and Open Apis: Nasas"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
tags:
- api
- python
- rest
- nasa
---
Alta3 Research and I often provide [Python](https://alta3.com/courses/api) training solutions for corporate enterprises. The video below is one I made documenting the use of Python to interact with NASA’s Open APIs by leveraging the HTTP / HTTPS protocol.

<iframe width="560" height="315" src="https://www.youtube.com/embed/UetUm0q0sBE?si=U_u2P1akpjiJdmgM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


Using Python to interact with APIs using HTTP / HTTPS is a critical skillet for anyone looking to automate solutions within their infrastructure. In this lab, the messaging is rather simple. Of the many HTTP / HTTPS verbs (POST, DELETE, PUT, etc.), we’ll only be utilizing the GET. Consider the model below.

    (our code)
    Python Client ------- HTTPS GET -------> NASA API
                  <------ 200 + JSON -------


The Python client handles the crafting and sending of the HTTPS GET message to NASA’s APIs. A GET is actually the same message your browser typically sends when requesting HTML data from a web-server.

Our code must also allow processing of the 200 HTTPS response, as well as the attached JSON. Again, a 200 is the same message your browser would expect to receive when interacting with a web-server. However, rather than processing HTML, we’ll be processing JSON.

What is JSON? It’s just a simple way to serialize data. I should do a blog post on it sometime, but for now, check out the IETF’s whitepaper (standard) on [The JavaScript Obejct Notation Data Interchange Format (RFC 7159)](https://tools.ietf.org/html/rfc7159). It is quite a bit like Python lists and dictionaries, so you might also try checking out [my post on Python dictionaries](https://www.rzfeeser.com/blog/python-training-python-dictionaries).

#### What is a “NASA Open API”?

In addition to putting people on the moon, and going to Mars, NASA helps the people of the world better understand science. Learning to work with APIs, and the data-sets they can expose, has quickly become core tenant of STEM fields, which is why we find NASA hosting “open APIs”.

The terms “API”, “open”, “rest”, and “restful”, are vague terms. In this case, NASA has created an open data-set that is free to use. Per the [NASA API wesbite](https://api.nasa.gov/), “**You do not need to authenticate** in order to explore the NASA data. However, if you will be intensively using the APIs to, say, support a mobile application, then you should sign up for a [NASA developer key](https://api.nasa.gov/#signUp)“

Signing up for a developer key is rather painless, and free. Besides sounding mighty impressive, it will allow you to use the [APIs](https://alta3.com/courses/api) more rapidly without experiencing “rate limiting”.

If you get stuck or lost, make a comment on the YouTube video, and I’ll be sure to follow up with a solution!

This post orginally appeared at [rzfeeser.com](https://www.rzfeeser.com/blog/python-and-apis-nasas-open-apis) and has been reproduced with the permission of the author.
