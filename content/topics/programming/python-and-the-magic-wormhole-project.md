---
title: "Python And The Magic Wormhole Project"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
---
The **Magic-Wormhole** is a [Python](https://alta3.com/courses/pyb) project that seeks to send files from one file to another, safely. There is quite a bit of research on securely sending files between computers, but it’s never been made easy. The wormhole seeks to fix this.  

**File Transfer Options**

You might be saying, “I already have a few tools to push files around." Let’s explore why, most of those tools are actually horrid:

- **Send email** – You’ll need to type at least 30 characters, likely need a GUI client, and subject to eavesdroppers like ISPs, CAs, and other internet lurkers. Companies will often times strip off attachments if they don’t like the extension type.

- **Upload FTP/HTTP** – The receiver of the file will need a minimum of 60 characters, and still subject to ISPs, CAs, and trolls.

- **Dropbox** – The receiver of the file will need a minimum of 60 characters, and subject to eavesdroppers like Dropbox (imagine that), and CAs. Corporations will often severely limit (or eliminate) using Dropbox.

- **URL Shorteners** – The receiver of the file will need a minimum of 20 characters, and opens up your attacker list to include lucky guessers, the shortening service, Dropbox, and CAs.

- **USB Drive** – You’ll need to pass a physical medium, but, eww gross.

- **SSH/scp** – The receiver needs a public-key that is about 740ish characters in length. This is easily transferred by the sender with ssh-copy-id and besides needing to push this key, has no other downsides.

**The Wormhole** – Requires the receiver to know about 20 characters, which are typically a combo of an integer and two short words. It is usually a bit silly, like 9-cowboys-iceskating. Other than installing the wormhole, there are no other shortcomings with this method.

The wormhole uses PAKE “Password-Authenticated Key Exchange”. In short, it’s a family of cryptographic algorithms that uses a short low-entropy password to establish a strong high-entropy shared key. The shared key is then used to encrypt data. To keep it secret and safe, the SPAKE2 algorithm is employed thanks to Abdalla and Pointcheval1.

Read more about the project here: https://magic-wormhole.readthedocs.io/en/latest/

So, how does one use the magic powers of the magic-wormhole? It is easy! It starts with the sender describing the file they want to move.

```
Sender:
% wormhole send rzfeeser.md
Sending 7924 byte file named 'rzfeeser.md'
On the other computer, please run: wormhole receive
Wormhole code is: 3-turtles-running

Sending (<-10.0.1.43:58988)..
100%|=========================| 7.92K/7.92K [00:00<00:00, 6.02MB/s]
File sent.. waiting for confirmation
Confirmation received. Transfer complete.
```
    
Now it is just up to the receiver of the file to activate the wormhole, and type in the code provided to the sender.

```
Receiver:
% wormhole receive
Enter receive wormhole code: 3-turtles-running
Receiving file (7924 bytes) into: rzfeeser.md
ok? (y/n): y
Receiving (->tcp:10.0.1.43:58986)..
100%|===========================| 7.92K/7.92K [00:00<00:00, 120KB/s]
Received file written to rzfeeser.md
```
    
Installing the application is super simple:

**Debian/Ubuntu**
`$ sudo apt install magic-wormhole`

**Fedora**
`$ sudo dnf install magic-wormhole`

**openSUSE**
`$ sudo zypper install python-magic-wormhole`

**Linux (Snap packages)**
Many linux distros can install a “snap” package. If that’s your thing, you can use the following command:
`$ sudo snap install wormhole`

**Install with Python 3.x**
Magic-wormhole is a Python package. To install it, you can type the following command, the only “tweak” might be changing the word “python3” to “python”. Use whatever command word you use to call on your install of [Python](https://alta3.com/courses/pyb).
`$ python3 -m pip install magic-wormhole --user`

Good luck throwing files into the wormhole!
