---
title: "Introduction To Kubernetes"
date: 2024-02-22
draft: false
headingPost: "Author: Stu Feeser"
---

Stu Feeser introduces [Kubernetes](https://alta3.com/courses/kubernetes): 
{{< youtube uzxSZqSqiLk >}}
