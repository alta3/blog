---
title: "Installing Rancher with ansible - Lesson 1"
date: 2025-02-13
draft: false
headingPost: "Author: Stu Feeser"
---

### Getting started

The first step in planning an ansible playbook is to record the steps to be automated. Almost always, that means you need to write a bach script, or at least the bash steps.


## Start the Lab Environment


### Objective
Here is the demo environment we will be using to start this Rancher Kubernetes journey! Our goal is to automate these steps using ansible. Doing so lays down the obvious requirement that we understand the manual installation first.  Let's get started with that. 

<img src="https://labs.alta3.com/courses/rancher/images/rancher/Slide1.PNG" />

>  If you want to try this yourself, all you will need is **two** machines:  
    1. A "bchd" machine where you will run ansible  
    2. A "target" machine where you will be installing Rancher  


#### In this demonstration, your instructor will:

  ✅ Run the lastest Rancher container.  
  ✅ Obtain the **Bootstrap Password**.  
  ✅  Explain the bash commands.  
  ✅ Write an ansible playbook to automate the above bash steps.  

### Demonstration

1. Install rancher on a node called **controller-1**. `ssh` to that node.

    `student@bchd:~$` `ssh controller-1`
    
    > Type `yes` when prompted.

0. We need docker which will then run the rancher container, so install docker as follows.

   `student@controller-1:~$` `sudo apt install -y docker.io`

    <img src="https://labs.alta3.com/courses/rancher/images/rancher/Slide2.PNG" />

0. Start the rancher container.

    `student@controller-1:~$` `sudo docker run -d --restart=unless-stopped --name rancher -p 80:80 -p 443:443 --privileged rancher/rancher:v2.8.4`
    
    > Note that your rancher container will listen on TCP port 80 and 443. Also note that the container is running in privileged mode. This means the container is given full control over the host. As always, **WITH GREAT POWER COMES GREAT RESPONSIBILITY**.

0. Run the following command to view the logs of your rancher installation. Feel free to watch these logs for 10 seconds or so. They are showing you all of the steps that Rancher is taking to get itself set up. To exit type **Ctrl-c**.

    `student@controller-1:~$` `sudo docker logs -f rancher`

    > When you feel you have watched enough logs to get the feel, exit from watching the logs. To exit, press `Ctrl-c`.

0. You will need a password to connect to the rancher web interface and that password you need is IN THE LOGS. Grep for the password using the following command to save it into the file called **pswd.txt**.

    `student@controller-1:~$` `sudo docker logs rancher 2>&1 | grep "Bootstrap Password" | awk '{print $6}' | sudo tee pswd.txt`


### The bash script:
Here is the bash script that performs the above tasks:

<details><summary> -- CLICK for the bash Script -- </summary>
  
  ```bash
  ssh controller-1 sudo apt install -y docker.io
  ssh controller-1 sudo docker run -d --restart=unless-stopped --name rancher -p 80:80 -p 443:443 --privileged rancher/rancher:v2.6.9
  echo
  echo sleeping 120 seconds from
  date
  sleep 120
  ssh controller-1 sudo docker logs rancher 2>&1 | grep "Bootstrap Password" | awk '{print $6}' | sudo tee pswd.txt
  ```
</details>



## Ansible automation:
Here is an asible playbook that automates those bash steps from above. The full explaination of the playbook below is in Chad's video:

<details><summary> -- CLICK for Playbook EXAMPLE --  </summary>

  ```bash
  #!/bin/bash
  
  # Create the inventory file
  cat << EOF > inventory.ini
  [rancher]
  controller-1 ansible_user=student ansible_become=true
  EOF
  
  # Create the playbook file
  cat << EOF > install_rancher.yml
  - name: Install Rancher on controller-1
    hosts: controller-1
    become: true
    tasks:
      - name: Install Docker
        apt:
          name: docker.io
          state: present
          update_cache: yes
  
      - name: Start and enable Docker service
        systemd:
          name: docker
          state: started
          enabled: yes
  
      - name: Run Rancher container
        command:
          cmd: >
            docker run -d --restart=unless-stopped --name rancher 
            -p 80:80 -p 443:443 --privileged rancher/rancher:v2.6.9
        args:
          creates: /var/lib/docker/containers/rancher
        register: rancher_run
        changed_when: rancher_run.rc == 0
  
      - name: Wait for Rancher to initialize
        pause:
          seconds: 120
  
      - name: Get Rancher Bootstrap Password
        shell: |
          docker logs rancher 2>&1 | grep "Bootstrap Password" | awk '{print \$6}'
        register: rancher_password
        changed_when: false
  
      - name: Save Bootstrap Password
        copy:
          content: "{{ rancher_password.stdout }}"
          dest: /home/student/pswd.txt
          owner: student
          group: student
          mode: '0600'

  EOF
  
  # Run the playbook
  ansible-playbook -i inventory.ini install_rancher.yml
  ```

</details>


