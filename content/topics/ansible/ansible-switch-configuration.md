---
title: "Ansible Switch Configuration"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
tags:
- ansible
- eos-banner
- ansible_switch_configuration
- eos
- show
---
[Ansible](https://alta3.com/courses/ansible-101) is able to change the configuration of network devices. In this exercise, we will change the [configuration of an Arista Switch](https://alta3.com/courses/napya).

<iframe width="560" height="315" src="https://www.youtube.com/embed/NwhZ7eiHy0E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



### Step 1 -  <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=20s"> cat the ansible hosts file</a>

  `student@beachhead:` `cat hosts`
  
  ```
  172.16.2.10
  172.16.2.20
  ```

### Step 2 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=35s"> Check ssh connectivity to the first host. It works!</a>

`student@beachhead:` `ssh admin@172.16.2.10`

  ```
  Password :
  Last login: Wed Aug 22 19:13:24 2018 from 172.16.2.100 SW1>
  SW1>
  SWl>exit
  Connection to 172.16.2.10 closed.	
  ```
  
### Step 3 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=52s"> Check ssh connectivity to the second host. It works too!</a>

`student@beachhead:` `ssh admin@172.16.2.20`

  ```
  student@beachhead:-/net01$ 
  ssh admin@172.16.2.20 
  Password :
  Last login: Wed Aug 22 19:16:07 2018 from 172.16.2.100 
  SW2>
  SW2>exit
  Connection to 172.16.2.20 closed. 
  ```

### Step 4 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=123s"> cat the ansible playbook and study it line by line.</a>

  Ultimately, all this playbook is going to do is change the login banner, a very safe thing to do as a first attempt!
  
  `student@beachhead:$` `cat netOl.yml`
  
  ```
  - name: My Arista Playbook 
    hosts: all 
	gatherfacts: false
    vars:
      ansibleconnection: networkcli 
	  ansible network os: 
	  eos ansible_become: yes 
	  ansiblebecomemethod: enable 
	  ansible user: admin 
	  ansible_ssh_pass: alta3
	  
    tasks:
    - name: configure the login banner 
	  eosbanner: 
	    authorize: yes 
		banner: login 
		text: | 
		  Congrats! You set a banner using an Ansible EOS module. 
		  Notice how YAML formatting lets us do multi-line strings
		state: present  
  ```
  
### Step 5 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=440s"> Check out the EOS ansible module documentation</a>
  
  https://docs.ansible.com/ansible/2.10/collections/arista/eos/eos_banner_module.html  
  
  
### Step 6 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=478s"> Run the playbook. It works!</a>

  `student@beachhead:$` `ansible-playbook -i hosts netOl.yml`
  
  ```
  PLAY [My Arista Playbook] ****************************************************
  TASK [configure the login banner] ********************************************
  changed: [172.16.2.10]"
  changed: [172.16.2.20]
  
  PLAY RECAP ******************************************************************* 
  172.16.2.10	: ok=l	changed=1	unreachable=0	failed=0
  172.16.2.20	: ok=l	changed=l	unreachable=0	failed=0	
  ```
  
### Step 7 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=510s"> Confirm that the changes were made. Testing shows that it worked!</a>

  `student@beachhead:$` `ssh admin@172.16.2.10`

  ```
  Congrats! You set a banner using an Ansible EOS module 
  Notice how YAML formatting lets us do multi-line strings 
  Password:
  ```

  `student@beachhead:$` `ssh admin@172.16.Z-20`

  ```
  Congrats! You set a banner using an Ansible EOS module 
  Notice how YAML formatting lets us do multi-line strings 
  Password:
  ```
 
 ### Step 8 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=534s"> Demonstrate the power of ansible by making a single parameter change.</a>
 
   ```
  - name: My Arista Playbook 
    hosts: all 
	gatherfacts: false
    vars:
      ansibleconnection: networkcli 
	  ansible network os: 
	  eos ansible_become: yes 
	  ansiblebecomemethod: enable 
	  ansible user: admin 
	  ansible_ssh_pass: alta3
	  
    tasks:
    - name: configure the login banner 
	  eosbanner: 
	    authorize: yes 
		banner: login 
		text: | 
		  Congrats! You set a banner using an Ansible EOS module. 
		  Notice how YAML formatting lets us do multi-line strings
		state: present  # <----- CHANGE THIS LINE TO state: absent
  ```
 
 ### Step 9 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=560s"> Run the playbook again.</a>

  `student@beachhead:$` `ansible-playbook -i hosts netOl.yml`
  
  ```
  PLAY [My Arista Playbook] ****************************************************
  TASK [configure the login banner] ********************************************
  changed: [172.16.2.10]"
  changed: [172.16.2.20]
  
  PLAY RECAP ******************************************************************* 
  172.16.2.10	: ok=l	changed=1	unreachable=0	failed=0
  172.16.2.20	: ok=l	changed=l	unreachable=0	failed=0	
  ```
  
 ### Step 10 - <a target="_blank" href="https://www.youtube.com/watch?v=NwhZ7eiHy0E&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=3&t=574s"> Confirm that the changes were made. No more login banner.  It worked!</a>

  `student@beachhead:$` `ssh admin@172.16.2.10`

  ```
  Password:
  ```
