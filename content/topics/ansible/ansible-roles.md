---
title: "Making an ansible role"
date: 2024-04-12
draft: false
headingPost: "Author: Stu Feeser"
tags:
- ansible
- roles
---
![image](/images/roles.webp)

**A DevOps chef making Ansible "ROLES"**

[Ansible](https://alta3.com/courses/ansible-101)'s power and simplicity make it a favorite tool among automation professionals. Yet, as infrastructure demands grow and playbooks become more complex, maintaining these automation scripts can become challenging. Ansible roles are designed to address this by packaging automation tasks into reusable, maintainable units. This blog will explore how to manually create [Ansible](https://alta3.com/courses/ansible-101) roles, bypassing tools like Ansible Galaxy for those who prefer a more hands-on approach.

## What Are Ansible Roles?

Ansible roles are essentially frameworks for breaking down complex automation scripts into manageable, reusable components. Each role can include variables, tasks, files, templates, and more, allowing you to organize and [scale your automation efforts efficiently](https://alta3.com/courses/mans).

## Benefits of Using Ansible Roles

1. **Reusability**: Roles allow you to reuse automation logic across different projects and environments, reducing redundancy.
2. **Modularity**: By dividing tasks into roles, you can manage complex playbooks more easily, making them more understandable and maintainable.
3. **Organization**: Roles help organize your automation scripts into a clearly structured format.
4. **Collaboration**: Packaged roles can be shared with other team members, fostering collaboration and consistency across projects.

## Creating a Role Manually

Here's how you can create an [Ansible](https://alta3.com/courses/ansible-101) role manually using basic command-line operations without relying on Ansible Galaxy:

### Step 1: Define the Role Structure

To initialize a new role in Ansible, you use the ansible-galaxy command. This tool is part of the Ansible ecosystem and is used primarily for creating and managing Ansible roles that can be shared and used across different projects.

```bash
ansible-galaxy init webserver
```

This command sets up the following directories:
- `tasks`: Contains the main list of tasks that the role will execute.
- `handlers`: Stores handlers, which are tasks that run in response to notifications.
- `defaults`: Holds default variables for the role.
- `vars`: Where you can define other variables that shouldn't be overridden as easily as defaults.
- `files`: For any files the role needs to transfer.
- `templates`: For templates that will be processed by Ansible.
- `meta`: For metadata about the role, such as dependencies.

### Step 2: Populate the Role

#### Tasks

Create a `main.yml` file inside the `tasks` directory where you will define what the role should do. Here’s an example for installing and starting Nginx:

```yaml
---
- name: Install nginx
  apt:
    name: nginx
    state: present

- name: Ensure nginx is running
  service:
    name: nginx
    state: started
    enabled: yes
```

Save this in `webserver/tasks/main.yml`.

#### Variables

In `webserver/defaults/main.yml`, define some default variables:

```yaml
---
http_port: 80
max_clients: 200
```

#### Templates

If your role uses configuration templates, you would place them in the `templates` directory and reference them in your tasks like so:

```yaml
- name: Configure nginx
  template:
    src: nginx.conf.j2
    dest: /etc/nginx/nginx.conf
```

### Step 3: Using Your Role in a Playbook

Include the role in your playbook like this:

```yaml
---
- hosts: webservers
  roles:
    - webserver
```

## Best Practices

- **Single Responsibility**: Keep roles focused on a single purpose.
- **Use Variables for Customization**: Make roles adaptable without altering their code by using variables.
- **Independently Test Roles**: Ensure each role functions correctly on its own.
- **Document Roles**: Write clear documentation for each role to make them easier to use and maintain.

Explore using Ansible roles in our [Ansible 101: Essentials](https://alta3.com/courses/ansible-101) course. 
