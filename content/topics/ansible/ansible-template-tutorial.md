---
title: "Ansible Template Tutorial"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
tags:
- ansible_template
- ansible_template_module
---
In the following youtube video, Zach Feeser demonstrates how to use the [Ansible](https://alta3.com/courses/ansible-101) template module.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZDwkus-0A3I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

We have or three hosts--Fry, Bender, and Zoidberg--that we will be configuring. To do so, we will be running an [Ansible](https://alta3.com/courses/ansible-101) playbook from our localhost (the Ansible controller), filling in the blanks of a jinja2 template.

![ansible-template-module](https://static.alta3.com/images/ansible/templates/ansible-template-module.png)

The six step process is as follows:

### Step 1 - <a target="_blank" href="https://www.youtube.com/watch?v=ZDwkus-0A3I&t=174s"> Define the template </a>

By definition, a template is a document with intentional ommisions that will be filled in by the template module. Zach uses a configuration file as an example. Here is the file he used:

`ship.cfg.j2`  

  ```
  [default]
  name = {{ shipname }} 
  type = {{ shiptype }} 
  engine type = {{ engines }}
  Darkmatter maxcapacity = {{ dark Matter balls }} 
  salesprice = {{ ship priceinbeans }} 
  anti matter afterburner = true
  ```

### Step 2 - <a target="_blank" href="https://www.youtube.com/watch?v=ZDwkus-0A3I&t=52s"> Define the key-value pairs that will "fill in the blanks"</a>

  ```
  vars:
     shipname: "Bessie" 
     shiptype: "Intergalactic" 
     engines: "Dark Matter" 
     darkMatterballs: 63 
     shippriceinbeans: 2
  ```   

### Step 3 - <a target="_blank" href="https://www.youtube.com/watch?v=ZDwkus-0A3I&t=100s"> Create a template task</a>

  ```
  tasks:
  - name: "Configure space ship config file"
    template:	
      src: /home/student/templates/ship.cfg.j2    # Location of the source template
      dest: -/ship.cfg                            # name of the completed file to be placed on the target system
  ```
    
### Step 4 <a target="_blank" href="https://www.youtube.com/watch?v=ZDwkus-0A3I&t=0s"> Create a playbook</a>

  `module-template.yml`
    
  ```
  - hosts: planet-express
  
    vars:
       shipname: "Bessie" 
       shiptype: "Intergalactic" 
       engines: "Dark Matter" 
       darkMatterballs: 63 
       shippriceinbeans: 2    

    tasks:
    - name: "Configure space ship config file"
      template:	
        src: /home/student/templates/ship.cfg.j2    # Location of the source template
        dest: -/ship.cfg                            # name of the completed file to be placed on the target system
  ```
    
### Step 5 <a target="_blank" href="https://www.youtube.com/watch?v=ZDwkus-0A3I&t=18s"> Define the target hosts (aka: The inventory)</a>

  ```
  [planet-express]
  bender    ansible host=10.10.2.3 ansible_ssh_user=bender 
  fry       ansible host-10.10.2.4 ansible_ssh_user-fry
  zoidberg  ansible host=10.10.2.5 ansible_ssh_user=zoidberg
  ```
 
 ### Step 6 <a target="_blank" href="https://www.youtube.com/watch?v=ZDwkus-0A3I&t=318s"> Run the playbook and observe the results</a>

At the end of the video, Zach runs the [Ansible](https://alta3.com/courses/ansible-101) playbook and then connects to each machine and shows that the template module has created a `ship.cfg` file that looks like this:

`ship.cfg`

  ```
  [default]
  name = Bessie
  type = Intergalactic
  engine type = Dark Matter
  Darkmatter maxcapacity = 63
  salesprice = 2
  anti matter afterburner = true
  ```
