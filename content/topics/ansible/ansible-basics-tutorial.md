---
title: "Ansible Basics Tutorial"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
tags:
- hosts
- ansible_hosts
- ansible_inventory
- ansible_playbook
- ansible_tutorial
- playbook
- versions
---
In the following youtube video, Zach Feeser demonstrates how to use [Ansible](https://alta3.com/courses/ansible-101) to configure remote hosts.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1rviSZx446Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Step 1 - <a target="_blank" href="https://www.youtube.com/watch?v=1rviSZx446Y&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=1&t=0s">Inventory</a>

  `student@beachhead:` `cat futuramacrew`
  
  ```
  [planet-express]
  bender	  ansible_host=10.10.2.3	ansible	ssh_user=bender
  fry	      ansible host=10.10.2.4	ansible	ssh user=fry
  zoidberg	ansible_host=10.10.2.5	ansible	ssh_user=zoidberg
  ```
### Step 2 - <a target="_blank" href="https://www.youtube.com/watch?v=1rviSZx446Y&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=1&t=143s">Confirm SSH Connectivity to inventory hosts. It works!</a>

  `student@beachhead:` `ssh bender@10.10.2.3`
  
  ```
  Welcome to Ubuntu 18.04 LTS (GNU/Linux 4.4.0-119-generic x86 64)
  
  • Documentation: https://help.ubuntu.com
  • Management:	https://landscape.canonical.com
  • Support:	https://ubuntu.com/advantage
  
  This system has been minimized by removing packages and content that are
  not required on a system that users do not log into.
  
  To restore this content, you can run the 'unminimize' command.
  Last login: Wed Aug 22 17:48:24 2018 from 10.11.2.1
  ```
### Step 3 - <a target="_blank" href="https://www.youtube.com/watch?v=1rviSZx446Y&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=1&t=218s"> Line by line analysis of a basic playbook</a>

  `student@beachhead:` `cat apt-sl.yml`
  
  ```
  - hosts: planet-express 
    gather facts: True # Default is True
    tasks:
    - name: "apt Module - Install packages" 
      apt:
        name: sl
        state: present  # Change to absent to remove 
        become: True
   ```

### Step 4 - <a target="_blank" href="https://www.youtube.com/watch?v=1rviSZx446Y&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=1&t=810s">Run the playbook</a>

  `student@beachhead:` `ansible-playbook -i futuramacrew apt-sl.yml`
  
  ```
  PLAY [ planet-express ] ****************************************************
  
  TASK [Gathering Facts]  ******************************************************
  ok: [bender]
  ok: [fry]
  ok: [zoidberg]
  
  TASK [apt Module - Install packages] *****************************************
  changed: [fry]
  changed: [bender]
  changed: [zoidberg]

  PLAY RECAP **********************************************************************
  bender       : ok=2 changed=l	unreachable=0	failed=0
  fry          : ok=2 changed=l	unreachable=0	failed=0
  zoidberg     : ok=2 changed=l	unreachable=G	failed=0
  ```

### Step 5 - <a target="_blank" href="https://www.youtube.com/watch?v=1rviSZx446Y&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=1&t=890s">Run the playbook again</a>
Note that this time, the apt module does not make changes.  [Ansible](https://alta3.com/courses/ansible-101) determines that the apt install has aleady been completed, so it's not necessary to run module again.


  `student@beachhead:` `ansible-playbook -i futuramacrew apt-sl.yml`
  
  ```
  PLAY [ planet-express ] ****************************************************
  
  TASK [Gathering Facts]  ******************************************************
  ok: [bender]
  ok: [fry]
  ok: [zoidberg]
  
  TASK [apt Module - Install packages] *****************************************
  ok: [fry]
  ok: [bender]
  ok: [zoidberg]

  PLAY RECAP **********************************************************************
  bender       : ok=2 changed=l	unreachable=0	failed=0
  fry          : ok=2 changed=l	unreachable=0	failed=0
  zoidberg     : ok=2 changed=l	unreachable=G	failed=0
  ```


### Step 6 - <a target="_blank" href="https://www.youtube.com/watch?v=1rviSZx446Y&list=PLleCw-vqe90At4NBfBu1OwaOcAOScocy2&index=1&t=958s">Confirm that ansible installed sl. It worked!</a>

  ![Steam Locomotive](https://static.alta3.com/images/sl.png)
