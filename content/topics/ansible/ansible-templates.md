---
title: "Ansible Templates"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
tags:
- jinja2
- ansible_template
- ansible_template_module
---


### Lab Objective

There are two ways to set up inventories for your [Ansible](https://alta3.com/courses/ansible-101) playbooks: the smart way and the not-so-smart way. We'll learn how the inventory directory works and what you should AVOID during set up.

### Process

1. Let's establish good habits for how we organize our TMUX environment for developing playbooks. Split your terminal so you have four panes (the TMUX commands you'll want to know are below)

    **SPLIT SCREEN HORIZONTALLY:**
    ctrl b (hands off keyboard) shift "

    **SPLIT SCREEN VERTICALLY:**
    ctrl b (hands off keyboard) shift 5

    **SWITCH BETWEEN PANES:**
    ctrl b (hands off keyboard) arrow key
    
0. When you've split your terminal, it should look like this. The panes have been labeled to demonstrate what each pane will be used for.

    ```
    +----------------------------------------------+
    |                      |                       |
    |  PANE 1              |  PANE 2               |
    |  "Tree" output       |  Command line         |
    |                      |                       |
    |                      |                       |
    +----------------------------------------------+
    |                      |                       |
    |  PANE 3              |  PANE 4               |
    |  Host files          |  Playbook             |
    |                      |                       |
    |                      |                       |
    +----------------------------------------------+
    ```

0. In PANE 1, create a new directory for this lab.

    `PANE 1 ~$` `mkdir -p ~/smurfdir && cd ~/smurfdir`

0. Here we are going to make our host file using the file content below.

    `PANE 1 ~/smurfdir$` `vim hosts`

    ```
    all:
      hosts:
      children:
          webserver:
              hosts:
                  student1
    ```

0. We will now make three subdirectories, *groups_vars, host_vars* and *templates*.

     `PANE 1 ~/smurfdir$` `mkdir group_vars host_vars templates`

0. Let's create a file called *webserver* inside our group_vars directory.

    `PANE 1 ~/smurfdir$` `vim group_vars/webserver`

    ```
    smurfs: little pink people
    ```

0. Stifle your outrage for now--of COURSE Smurfs are not pink--but it's all part of the point we are trying to make. Create another file called *student1* inside of *host_vars*.

    `PANE 1 ~/smurfdir$` `vim host_vars/student1`

    ```
    ansible_host: 192.168.6.18
    smurfs: lil blue guys
    user: ubuntu
    ```

0. BLUE Smurfs, that's better. Inside of your templates directory, create a file named *smurfcolor*. *{{ smurfs }}* is declaring that *smurfs* is a variable, and that when this template is executed the value of *smurfs* should take its place.

    `PANE 1 ~/smurfdir$` `vim templates/smurfcolor`

    ```
    Smurf Report: {{ smurfs }}
    ```

0. Last, let's install and run a helpful program named *tree* to help us visualize our files and subdirectories.

    `PANE 1 ~/smurfdir$` `sudo apt install tree`

    `PANE 1 ~/smurfdir$` `tree`

0. Your display should now look like this:

    ```
    .
    ├── group_vars
    │   └── webserver
    ├── hosts
    ├── host_vars
    │   └── student1
    └── templates
        └── smurfcolor
    ```

0. Switch to PANE 3 (bottom left) and let's have our *webserver* and *student1* files open and visible to us.

    `PANE 3 ~$` `vim /home/student/smurfdir/host_vars/student1`

0. Now that you are in vim, we can open multiple files in vim as tabs! Enter this command while in command mode:

    `:tabe /home/student/smurfdir/group_vars/webserver`

0. Cool! You now have a very 1990s looking tab structure at the top of this pane. Press `gt` to tab back and forth between *webserver* and *student1* in PANE 3.

0. We're now ready to set up our playbook. Move to PANE 4 and create our *smurfmaker.yaml* playbook.

    `PANE 4 ~$` `vim /home/student/smurfmaker.yaml`

    ```
    - name: smurf color identification
      hosts: webserver
      remote_user: ubuntu

      tasks:
      - name: create a file
        template:
          src: smurfcolor
          dest: /home/student/smurfreport
          owner: "{{ user }}"
          mode: '0644'
    ```

0. DO NOT leave vim in PANE 4! As soon as you write-**quit**, you will lose all your `undo` abilities! Whenever you make a change to *smurfmaker.yaml*, use `:w` only.

0. Move to PANE 2. It's time for us to call on this playbook, which will generate a file called *smurfreport* in /home/student. We are using *smurfcolor* as our template, which will replace the value of {{ smurfs }} when the new file is made. But which value will it take? Take an educated guess, then run the playbook.

    `PANE 2 ~$` `ansible-playbook smurfmaker.yaml`

0. Read out the file that was just created.

    `PANE 2 ~$` `cat smurfreport`

    ```
    Smurf Report: lil blue guys
    ```

0. Ok! The value of {{ smurfs }} in our template was replaced by value in *host_vars/student1*. If you look at our yaml inventory we made in step 4, *student1* is at the BOTTOM. When Ansible searches for the value of a key, it starts at the bottom and works its way up.

0. Let's make a change to *host_vars/student1*. In PANE 3, select *student1* file and comment out the line containing the *smurfs* key. Save with `:w`

    ```
    ansible_host: 192.168.6.18
    #smurfs: lil blue guys
    user: ubuntu
    ```

0. In PANE 2, run your playbook again.

    `PANE 2 ~$` `ansible-playbook smurfmaker.yaml`

0. Read out the file that was just re-created.

    `PANE 2 ~$` `cat smurfreport`

    ```
    Smurf Report: lil pink guys
    ```

0. PINK?? Blasphemy. But observe why this happened. Because *smurfs* is no longer defined by *student1*.  It is now being defined by the group that student1 belongs to, *webserver*.

   ## STOP. THINK. CONSIDER.
   Imagine that we are not dealing with just one variable being defined in multiple places, but dozens. Imagine removing a variable definition in one place only to find out it's being defined SOMEWHERE ELSE instead... and you don't know where! EXTREME CAUTION should be exercised when declaring variables, and at [Alta3](https://alta3.com/courses/ansible-101) we definitely do not recommend seeding your directory structure with multiple definitions. Pain and frustration is sure to follow!

0. To demonstrate one additional level of hierarchy, let's return to PANE 4 and make a change. Edit your playbook to look like this:

    ```
    - name: smurf color identification
      hosts: webserver
      remote_user: ubuntu

      vars:
        smurfs: lil green guys

      tasks:
      - name: create a file
        template:
          src: smurfcolor
          dest: /home/student/smurfreport
          owner: "{{ user }}"
          mode: 0644
    ```

0. Save with a `:w`, then return to PANE 2 to run your playbook one more time.

    `PANE 2 ~$` `ansible-playbook smurfmaker.yaml`

0. Read out the file that was just re-created.

    `PANE 2 ~$` `cat smurfreport`

    ```
    Smurf Report: lil green guys
    ```

0. You can see that variables defined in the [Ansible](https://alta3.com/courses/ansible-101) playbook itself override all variables set elsewhere.
