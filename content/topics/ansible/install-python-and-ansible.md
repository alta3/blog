---
title: "Install Python And Ansible"
date: 2024-02-22
draft: false
headingPost: "Author: R Zach Feeser"
---
Let's install the lastest [Python](https://alta3.com/courses/pyb) and [Ansible](https://alta3.com/courses/ansible-101) using the best installation technique.

### Objective:  
 1. Use `apt` for system wide installation  
 2. Use `python3 -m pip` for user  
 3. Set up a virtual environment  

### Tasks: 
1. Update and upgrade the apt repositories.

    `$` `sudo apt update -y`
    
    `$` `sudo apt upgrade -y`

0. Install Python 3.9. NEVER use pip or pip3 with **sudo**.  Let apt manage the system Python installs. Later when we use pip, it is ONLY for user installations.

    `$` `sudo apt install python3.9 -y`
    
0. Now let's make Python 3.9.1 our default python3 interpreter.

    `$` `sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 1`
    
0. Now check your Python version.

    `$` `python3 --version`
    
    ```
    Python 3.9.1
    ```
    **OR**
    ```
    Python 3.9.0+
    ```

0. Make sure that you have the latest version of *pip*.

    `sudo apt install python3-venv -y`
    
    `sudo apt install python3-pip -y`

    `python3 -m pip install wheel`

    `python3 -m pip install --upgrade pip wheel`

0. The last command issues an ominious WARNING that you need to add a PATH.  And indeed you do, and soon, but first let's check our .profile file and see why that PATH is not included. When we check, we see that it would only be included if the `$HOME/.local/bin` directory exists.  And it did not exist when .profile was run, but it does exist now!

    `~ $` `vim .profile`

    ```
    # set PATH so it includes user's private bin if it exists
    if [ -d "$HOME/.local/bin" ] ; then
        PATH="$HOME/.local/bin:$PATH"
    fi
    ```
    
0. Since we know the `$HOME/.local/bin` PATH will be added by the .profile file, we can "activate" the path by sourcing .profile.

    `~ $` `source .profile`

0. Check if the `$HOME/.local/bin` PATH was added:

   `~ $ echo $PATH`

0. Now, let's install Ansible. If you are running WSL2, this will take about 1 minute.  But if you are running WSL1, this takes about 45 minutes. Start over and install WSL2.

    `$` `python3 -m pip install ansible`
    
0. Confirm your [Ansible](https://alta3.com/courses/ansible-101) version.

    `$` `ansible --version`

    ```
    ansible 2.10.5
      config file = None
      configured module search path = ['/home/<name>/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
      ansible python module location = /home/<name>/.local/lib/python3.9/site-packages/ansible
      executable location = /home/<name>/.local/bin/ansible
      python version = 3.9.0 (default, Feb 15 2021, 09:54:54) [GCC 5.4.0 20160609]
    ```
