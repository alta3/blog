---
title: "BRIO setup Guide"
date: 2024-02-22
draft: false
headingPost: "Author: Tracy Wertz"
tags:
- video
- how_to
- youtube
---


{{< youtube TJq4tbiTZwY >}}

1. Download Logitech G Hub software

    [LOGITECH G HUB Download](https://www.logitechg.com/en-gb/innovation/g-hub.html)

2. Check device manager for imaging device and make sure the brio driver is updated. 

3. Run G Hub software

4. Logitech Best Software Settings

{{< youtube 39BLnFNsam8 >}}

5. How to record with 1080p OBS

{{< youtube FFWzuIIaWFE >}}

6. How to record with 4K OBS

{{< youtube DZnkyq4kqkE >}}
