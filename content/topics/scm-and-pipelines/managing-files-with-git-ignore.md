---
title: "Managing the Files you Push with .gitignore"
date: 2024-02-28
draft: false
headingPost: "Author: Sean Barbour"
---

[Git, the cornerstone of modern version control](https://alta3.com/courses/github), offers developers unparalleled flexibility in managing project changes. Yet, sometimes, the need arises to sideline certain files or folders from this meticulous tracking. Whether it's to dodge configuration files, ignore logs, or bypass temporary files, mastering the art of exclusion in Git can streamline your workflow significantly. Let's dive into the simple yet effective methods to keep specific files or folders out of your commits.

## The Art of Ignoring: Single Files with .gitignore

The `.gitignore` file is your first line of defense against cluttering your repository with unnecessary files. Here’s how to leverage it:

### Step 1: Crafting or Modifying `.gitignore`

Embark on this journey at the heart of your [Git](https://alta3.com/courses/github) repository. Here, either birth a new `.gitignore` file into existence or tweak an existing one.

### Step 2: Specify Your Unwanted File

It's time to blacklist your file. Add its name directly to `.gitignore`. For instance, to ignore `example.txt`, simply append:
```plaintext
example.txt
```
This line acts as a shield, keeping `example.txt` out of your commits.

### Step 3: Cement the Changes

With `.gitignore` updated, anchor your changes into your repository’s history:
```bash
git add .gitignore
git commit -m "Refine .gitignore to sideline example.txt"
git push origin master
```
Your repository is now primed to overlook changes to `example.txt`.

## Folder Forgiveness: Excluding Entire Directories

To extend this exclusion to a whole folder, the process mirrors that for a single file, with a slight twist in specification.

### Step 4: `.gitignore` Revisited

As before, navigate to your repository's core and either modify or create the `.gitignore` file.

### Step 5: Directory Dismissal

Direct your `.gitignore` to ignore an entire folder by listing it as follows:
```plaintext
/example_folder/
```
This line ensures that `example_folder` and its contents become invisible to your commits.

### Step 6: Solidify Your Intentions

Lock in your preferences with a commit:
```bash
git add .gitignore
git commit -m "Enhance .gitignore to overlook example_folder/"
git push origin master
```
The repository will now consistently ignore `example_folder`.

## An Important Caveat: Dealing with Past Commitments

Altering `.gitignore` does not affect files or folders already tracked in Git's history. To **retroactively** untrack a previously committed item, execute:
```bash
git rm --cached example.txt  # For untracking a file
git rm --cached -r example_folder/  # For untracking a folder
```
And follow up with a commitment to this change:
```bash
git commit -m "Cease tracking of example.txt or example_folder/"
git push origin master
```

By embracing these steps, you wield the power to tailor your project's tracking to perfection, ensuring your Git repository remains clean, efficient, and relevant. This is not just about ignoring files; it's about [optimizing your version control](https://alta3.com/courses/github) to reflect the true essence of your project's needs.
