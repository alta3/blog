---
title: "Back Up Your Organizations Github"
date: 2024-02-22
draft: false
headingPost: "Author: Sean Barbour"
tags:
- back_up
- vcs
---
If you are using [GitHub](https://alta3.com/courses/github), then you need to get cracking on backing up your repositories--or even better, your entire GitHub organization!  Assuming that you have the permissions, it's vital to do this.  If an outage or disaster strikes, you want the recovery to go as smoothly as possible. This article applies if: 

 * You are an individual/organization who has a repo on GitHub where removal of that repo would in any way affect the status of operations.
 * You want to back up EVERY project in your GitHub account.
 * You know that your VCS (Version Control System) is the life blood or your company's Information System
 
Sound accurate? If so, then consider:
 
 * Where do you want to save these projects? (for example, a local datastore)
 * When do you want to back up these repositories? (Automation/manual hazard backup included)  
 
I was going through my company's infrastructure a few weeks back in an attempt to clean up and reorganize.  That's when I realized our [GitHub](https://alta3.com/courses/github) repositories hadn't been backed up in, uh...let's just say 'a while.'  The reality was that our repositories were dangling over the abyss, held aloft only by unbroken access to the internet and unbroken GitHub systems! YIKES. Sure, one might throw around words like "overreacting" and "paranoia" in my direction, but seriously...what if my fears became reality, and I hadn't backed up these repositories? Let's fix this problem.

0. First you need to navigate to your organization's GitHub account and log in. Once logged in, click on your user icon in the top right corner in order to access the dropdown menu and click on *Settings*.  
    
    ![2](https://static.alta3.com/blog//github-devsettings.PNG)  
    
0. Click on *Developer Settings.*  
    
    ![3](https://static.alta3.com/blog//github-devsettings2.PNG)  
        
0. Click on *Personal Access Tokens.*  
    
    ![4](https://static.alta3.com/blog//github-pats.PNG)  

0. Click *Generate new token*.  
    
    ![5](https://static.alta3.com/blog//github-gen-token.PNG)  

0. You might be asked to enter your GitHub password. Enter if asked.  

0. Put a small note in the text box referring to what the token is used/needed for.  

    ![6](https://static.alta3.com/blog//github-new-pat.PNG)  

    > I ended up making this a root access token, so make sure you check all the boxes in order to ensure access to every repository when running the backup. There are so many options here. Make sure you select the correct settings according to your company's policies and procedures. One way I deviate from a full analogous download of our GitHub Organization would be the options I set above--ESPECIALLY making sure the delete option is unchecked!  
    
    ![7](https://static.alta3.com/blog/github-save-token2.PNG)  

0. Click on the clipboard icon to copy the access token. You will not be able to see this again once you change screens. **Important, click 'copy' or highlight and copy this token as you will only be able to see this once in GitHub!**  

0. Now, it's time to get your backup started.  

    `mkdir github-backup`  

    `cd github-backup`  

    `pip install --user github-backup`  
    
0. Create a file and add your copied token to it. It will look similar to the code block below. **Do not copy the original shown here, it will not work.**  

    `vi token`  

        123457891@578hfredbdct32222282020help

    `export ACCESS_TOKEN=123457891@578hfredbdct32222282020help`  

    `github-backup --token $ACCESS_TOKEN --organization --repositories --private alta3`  

0. Check your results! Verify that all of your repositories exist and confirm the files within them.  

    ![8](https://static.alta3.com/blog///github-backup-results.PNG)  

## In Summary
We were able to create an access token that we can use for a variety of needs. In our case, this gives us the ability to access our GitHub organization remotely from a machine of our choosing. This means if you have access to that token, you have access to your team's GitHub repositories. YAY! The second item we accomplished was actually backing up our GitHub organization as a whole. The reason this is important to me is two-fold. First, I am not confident that GitHub will always be there in times of need. The second reason is it's good to have a backup that you can fully recover from in the absence of a service like [GitHub](https://alta3.com/courses/github).  
