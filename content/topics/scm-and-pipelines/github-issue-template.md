---
title: "Creating Github Issue Templates"
date: 2024-02-29
draft: false
weight: 29
headingPost: "Author: Sean Barbour"
---


Creating issues in [Github](https://alta3.com/courses/github) is **easy**. Given a repo's designation, you may even be able to create issues on other user's or organization's pages. However, when you have a team of people creating issues all the time, each individual will likely have their own style of creating issues. This tends to cause confusion from time to time between the end user(s) and folks actually working the ticket. I'm not going to tell anyone this is a must, but since creating templates for different scenarios, I have seen improvement in efficiency--all the way to closing the ticket. A little bit more on what templates are and why you might want to try them out:

### What are Github Issue Templates?

[GitHub Issue Templates](https://alta3.com/courses/github) streamline collaboration and communication within your repository. This is done by providing a structured format for issue creation and back and forth discussion on issues. In the below instruction set, I'll walk you through the process of creating a GitHub Issue Template for your repository. It's pretty straightforward. I'll be using [alta3/labs](https://github.com/alta3/labs) as an example.

### Why Use GitHub Issue Templates?

Let's briefly discuss the importance of GitHub Issue Templates. They serve as a standardized way for contributors to create tickets, provide essential details, and ensure that important information is **not** overlooked. Templates enhance the efficiency of issue tracking, making it easier for maintainers, like myself, and other contributors to understand and address problems or propose changes.

### Creating a GitHub Issue Template for alta3/labs

1. **Navigate to the Repository:**
   Visit your repository on GitHub. Again, the example here is a repo [alta3/labs](https://github.com/alta3/labs) we use daily at Alta3 Research.

2. **Go to the "Settings" Tab:**
   Click on the "Settings" tab in your repository's menu bar.

    ![image](/images/github-issue-template-1.PNG)

3. **Scroll down the page until you see the section called "Features":**
   On the right-hand side, click the green "Set up templates" button.

    ![image](/images/github-issue-template-2.PNG)

4. **Add a custom template**
   Click the dropdown "Add template: select", then click on "Custom template".

   ![image](/images/github-issue-template-3.PNG)

5. **Preview and edit**   
    Click on Preview and edit to customize your new template.

    ![image](/images/github-issue-template-4.PNG)

6. **Creat your custom issue template**
    Click on the little pencil in order to edit the template.

    ![image](/images/github-issue-template-5.PNG)

7. **Example from Alta3** Fields to fill in:

    - **Template Name**: What you see when you create a new issue with a template.
    - **About**: Short description of the purpose of said template.
    - **Template content**: The prefilled part that you will see upon creating a new issue with the template.
    - **Issue default title**: The default title unless you update it. Good to have guidelines here like in our example below. Tells the user what to put in for the title.
    - **Assignees**: Leave blank unless there is a particular individual that works on this type of issue. We have several and this template is more broad in scope.
    - **Labels**: A great way to help organize issues, detailing feature requests, needing assistance, bugs, development, etc.
    
    ![image](/images/github-issue-template-6.PNG)

8. **Save the Template**
    Click "Propose Changes" on the top right of your screen. A popup commit message will appear and you have the choice to commit to your *main* branch or start a PR so an administrator of the repo can approve the change. Assuming you have correct permissions, just click the green "Commit changes", adding whatever extended commit message that might further explain why a template is needed.

    ![image](/images/github-issue-template-7.PNG)

9. Now to see the template in action, click on the **Issues** menu tab at the top left of your screen. 

10. On the top right, click the green button **New Issue**.

11. You should now see a **Get started** button. Go ahead and click on it.

    ![image](/images/github-issue-template-8.PNG)

    > Side note: There are also links to create a blank issue (default github with no templating) **AND** Edit templates, where you can edit or create additional templates.

12. This is what the example template looks like when executing.

    ![image](/images/github-issue-template-9.PNG)






By following these steps, you've created a GitHub Issue Template tailored to your repository's needs. Contributors can now open issues using this template, ensuring that vital information is included and making the collaboration process smoother.

Remember, [GitHub Issue Templates](https://alta3.com/courses/github) are valuable tools for maintaining a structured and efficient issue tracking system. Whatever you're working on, templates enhance communication and streamline the problem-solving process.

