---
title: "Deploying AWS Infrastructure with Terraform"
date: 2025-02-28
draft: false
headingPost: "Author: Chad Feeser"
tags:
- terraform
- aws
- cloud_infrastructure
- devops
---

Using [Terraform](https://alta3.com/courses/terraform-101) to deploy infrastructure in AWS is *efficient* and *straightforward*. Below are the steps to launching an EC2 instance using Terraform.

{{< youtube RiBSzAgt2Hw >}}

### Step 1 - Install Terraform

Ensure you have Terraform installed on your system before proceeding. You can verify your installation using:

`terraform -v`

### Step 2 - Set Up Your Working Directory

Move to your home directory and create a new directory for your Terraform project:

```
cd ~
mkdir ~/aws-terraform-alta3 && cd ~/aws-terraform-alta3
```

### Step 3 - Export AWS Credentials

To avoid committing secrets, export your AWS credentials:

```
export AWS_ACCESS_KEY_ID="<YOUR_AWS_ACCESS_KEY_ID>"
export AWS_SECRET_ACCESS_KEY="<YOUR_AWS_SECRET_ACCESS_KEY>"
```

### Step 4 - Create the Terraform Configuration File

Create and edit `main.tf`:

```
vim main.tf
```

Paste the following configuration:

```terraform
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.24.1"
    }
  }
  required_version = ">= 0.15"
}

provider "aws" {
  region  = var.region
  profile = "default"
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"]
}

resource "aws_instance" "example" {
  ami                    = data.aws_ami.ubuntu.id
  key_name               = aws_key_pair.deployer.key_name
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sg_ssh.id]
  user_data              = <<-EOF
              #!/bin/bash
              apt-get update
              apt-get install -y apache2
              sed -i -e 's/80/8080/' /etc/apache2/ports.conf
              echo "Hello World" > /var/www/html/index.html
              systemctl restart apache2
              EOF
  tags = {
    Name          = "terraform-learn-state-ec2"
    drift_example = "v1"
  }
}

resource "aws_security_group" "sg_ssh" {
  name = "sg_ssh"
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```

Save and exit `vim` by pressing `ESC`, then typing `:wq`.

### Step 5 - Define Outputs

Create `outputs.tf`:

```
vim outputs.tf
```

Add the following:

```terraform
output "instance_id" {
  value = aws_instance.example.id
}

output "public_ip" {
  value       = aws_instance.example.public_ip
  description = "The public IP of the web server"
}

output "security_groups" {
  value = [aws_instance.example.vpc_security_group_ids]
}
```

Save and exit.

### Step 6 - Define Variables

Create `variables.tf`:

```
vim variables.tf
```

Add:

```terraform
variable "region" {
  description = "The AWS region your resources will be deployed"
}
```

Save and exit.

### Step 7 - Declare Variable Values

Create `terraform.tfvars`:

```
vim terraform.tfvars
```

Add:

```
region = "us-west-2"
```

Save and exit.

### Step 8 - Create an SSH Keypair

Run the following command, replacing `student@example.com` with your email:

```
ssh-keygen -t rsa -C "student@example.com" -f ~/.ssh/key
```

Press enter when prompted for a passphrase.

### Step 9 - Initialize Terraform

Initialize the project:

```
terraform init
```

### Step 10 - Deploy the Infrastructure

Apply your Terraform configuration:

```
terraform apply
```

If everything is correct, your AWS EC2 instance will be created successfully!

For more training on Terraform, check out the [Alta3 Research Terraform 101](https://alta3.com/courses/terraform-101) course overview. Training is available for individuals and companies looking for group training or certifications.


