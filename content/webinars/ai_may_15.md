---
title: "Building and Solving with AI"
date: 2024-05-15
draft: false
headingPost: "Author: Stuart Feeser"
weight: 1
---


<video width="100%"  controls poster="https://static.alta3.com/webpage-data/images/aiwebinar_05-15-2024.png">   >
  <source src="https://static.alta3.com/webpage-data/videos/AI-Webinar_2024-05-15.mp4" type="video/mp4">
  Building and Solving with AI
</video>


Reach out to us at info@alta3.com for inquiries about custom AI solutions and team training.