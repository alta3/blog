---
title: "REGEX" 
weight: 50
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheat_sheets
- regex
---

### Click image to download PDF

 <a href="https://static.alta3.com/posters/regex.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/regex.png" 
        alt="Git PDF" style="width: 85%;">

#### [regex.pptx](https://static.alta3.com/posters/regex.pptx)