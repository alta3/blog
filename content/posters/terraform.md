---
title: "Terraform" 
weight: 60
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheat_sheets
- terraform
---

### Click image to download PDF

 <a href="https://static.alta3.com/posters/terraform.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/terraform.png" 
        alt="Git PDF" style="width: 85%;">

#### [terraform.pptx](https://static.alta3.com/posters/terraform.pptx)