---
title: "Posters" 
weight: 10
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheet_sheets
---

### Click image to download PDF

----

 <a href="https://static.alta3.com/posters/k8s.pdf" target="_blank">
     <img src="https://static.alta3.com/posters/k8s.PNG" 
      alt="k8s PDF" style="width: 85%;">


----

 <a href="https://static.alta3.com/posters/ansible.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/ansible.png" 
        alt="Ansible PDF" style="width: 85%;">



----

 <a href="https://static.alta3.com/posters/git.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/git.PNG" 
        alt="Git PDF" style="width: 85%;">


----

 <a href="https://static.alta3.com/posters/python3.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/python3.png" 
        alt="Git PDF" style="width: 85%;">


----

 <a href="https://static.alta3.com/posters/python3pkgs.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/python3pkgs.png" 
        alt="Git PDF" style="width: 85%;">



----

 <a href="https://static.alta3.com/posters/regex.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/regex.png" 
        alt="Git PDF" style="width: 85%;">



----

 <a href="https://static.alta3.com/posters/sip.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/sip.png" 
        alt="Git PDF" style="width: 85%;">



----

 <a href="https://static.alta3.com/posters/terraform.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/terraform.png" 
        alt="Git PDF" style="width: 85%;">



----

 <a href="https://static.alta3.com/posters/vim.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/vim.PNG" 
        alt="Git PDF" style="width: 85%;">



----

 <a href="https://static.alta3.com/posters/yaml.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/yaml.PNG" 
        alt="Git PDF" style="width: 85%;">



----

 <a href="https://static.alta3.com/posters/tmux.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/tmux.PNG" 
        alt="Git PDF" style="width: 85%;">
