---
title: "Kubernetes Cheat Sheet" 
weight: 20
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheet_sheets
- kubernetes
---

### Click image to download PDF

 <a href="https://static.alta3.com/posters/k8s.pdf" target="_blank">
        <!-- Image that acts as the clickable link -->
        <img src="https://static.alta3.com/posters/k8s.PNG" alt="Ansible PDF" style="width: 85%;">


##### [PPTX Source](https://static.alta3.com/posters/k8s.pptx)
