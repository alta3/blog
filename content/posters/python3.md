---
title: "Python" 
weight: 40
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheat_sheets
- python
---

### Click image to download PDF

 <a href="https://static.alta3.com/posters/python3.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/python3.png" 
        alt="Git PDF" style="width: 85%;">

#### [python3.pptx](https://static.alta3.com/posters/python3.pptx)
