---
title: "TMUX" 
weight: 70
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheat_sheets
- tmux
---

### Click image to download PDF


 <a href="https://static.alta3.com/posters/tmux.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/tmux.PNG" 
        alt="Git PDF" style="width: 85%;">

#### [tmux.pptx](https://static.alta3.com/posters/tmux.pptx)