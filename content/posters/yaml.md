---
title: "YAML" 
weight: 90
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheat_sheets
- yaml
---

### Click image to download PDF

 <a href="https://static.alta3.com/posters/yaml.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/yaml.PNG" 
        alt="Git PDF" style="width: 85%;">

#### [yaml.pptx](https://static.alta3.com/posters/yaml.pptx)
