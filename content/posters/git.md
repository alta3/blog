---
title: "git" 
weight: 30
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheat_sheets
- git
---

### Click image to download PDF

 <a href="https://static.alta3.com/posters/git.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/git.PNG" 
        alt="Git PDF" style="width: 85%;">

#### [git.pptx](https://static.alta3.com/posters/git.pptx)