---
title: "VIM" 
weight: 90
alwaysopen: false
collapsibleMenu: "false"
tags:
- posters
- cheat_sheets
- vim
---

### Click image to download PDF


 <a href="https://static.alta3.com/posters/vim.pdf" target="_blank">
        <img src="https://static.alta3.com/posters/vim.PNG" 
        alt="Git PDF" style="width: 85%;">

#### [vim.pptx](https://static.alta3.com/posters/vim.pptx)