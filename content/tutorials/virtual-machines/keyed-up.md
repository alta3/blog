---
title: "Getting Keyed Up"
date: 2024-02-22
draft: false
weight: 22
headingPost: "Author: Stu Feeser"
tags:
- private_key
- id_rsa
---

We need <a target="_blank" href="https://en.wikipedia.org/wiki/RSA_(cryptosystem)"> RSA keys</a> in place to run the next series of labs. 

### Tasks:

1. Install an id+rsa keypair. It is **OK to answer NO** if the key already exists.

    `$` `ssh-keygen`

0. Note that ssh-keygen created *TWO* keys, one private, the other public.  Take a look at both of them:

    `$` `cat ~/.ssh/id_rsa`   This is the PRIVATE key. It effectively is your password. **NEVER** share this with anyone.

    `$` `cat ~/.ssh/id_rsa.pub`  This is your PUBLIC key. You can place it anywhere.

0. Next we'll want to copy your public RSA key. Run this command and COPY the output with your mouse.

    `$` `cat ~/.ssh/id_rsa.pub`  *Then copy the key with your mouse.*

0. Back in GitHub, click on your *profile* icon on the top-right corner.

0. Select **Settings** from the drop down menu.

0. Select **SSH and GPG Keys** on the left. NOTE: You should have as few keys as possible, ideally one (the one we're entering).

0. Click the green **New SSH Key** button.

0. Paste the key from your clipboard into GitHub.

0. Click the **Add** button.

0. You'll likely need to input your GitHub password to complete adding a new SSH Key.

0. Check your email and confirm that your key has been added. If your key was formatted incorrectly, it will be rejected immediately or via email.

0. Confirm that your new key is now in GitHub.

    `$` `curl https://github.com/sfeeser.keys`  
   
