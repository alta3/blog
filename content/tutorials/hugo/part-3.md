---
title: "Part 3: Custom Domain"
date: 2024-03-18
draft: false
headingPost: "Author: Joe Spizzandre"
weight: 53
---

![image](/images/custom-domain.png)

# Adding a Custom Domain to Your GitLab Pages Site: The Final Step

As we approach the conclusion of our journey to personalize your GitLab Pages site, we come to be exhilarating (and a bit daunting) crescendo, routing your site to a custom domain. But fear not! With a sprinkle of guidance and a dash of patience, you'll be showcasing your projects from a unique URL in no time, making your site more accessible and professionally branded.

### Step 1: The Gateway to Customization

First things first, let's venture into the realm of GitLab's documentation. Picture this as the treasure map leading us to our prized possession: our custom domain setup. Open a new tab and navigate to GitLab's official guide on [Custom Domains and SSL/TLS Certification](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/#steps). Keep this tab open; it's our compass for the journey ahead.

### Step 2: Unearthing the DNS Records

Following the steps laid out in GitLab's guide, we delve into our repository settings, seeking the mystical DNS records needed to bind our domain to our GitLab Pages site. These records come in two flavors:

- **CNAME or ALIAS Record:** Think of this as the secret handshake between your domain and GitLab Pages, telling the internet where to find your site.
- **TXT Record:** This is essentially your site's ID card, proving to the world (and GitLab) that your domain is indeed who it says it is.

### Step 3: Claiming Your Domain

Now, armed with our DNS records, we are ready to claim our domain. We will demonstrate the usage of services like AWS Route 53, which offers a wide selection of domains to choose from. However if you do not use AWS the instructions should be generic enough for you to follow along.

### Step 4: Navigating the AWS Console

The AWS Console, a landscape vast and wide, is where we'll plant our domain's flag. After logging in, we seek out the Route 53 service, our gateway to domain management.

### Step 5: The Ritual of DNS Configuration

With our domain selected, we're faced with two crucial tasks: summoning our CNAME and TXT records into existence.

#### Conjuring the CNAME Record:

- **Record name:** This will be a subdomain of your choosing, like `blog` in `blog.yourdomain.com`.
- **Value:** The URL, `https://<your-username>.gitlab.io`, points to your GitLab Pages.

#### Summoning the TXT Record:

- **Record name:** A specific incantation, `_gitlab-pages-verification-code.blog.yourdomain.com`.
- **Value:** The verification code, a secret passphrase provided by GitLab, proving your domain's allegiance. (You gathered this by following the instructions linked at the top of this post.

### The Final Act: Saving Your Progress

With both records meticulously added, a final click on the "SAVE" button seals our endeavor. Like the closing of a book, our setup is complete, but the story of your custom GitLab Pages site is just beginning to unfold. It can take about a minute for everything to be properly set up and your site to become viewable. 

### In Conclusion

What we've embarked on today is more than just technical configuration; it's a rite of passage in the digital world, marking your site with the badge of personalization and professionalism. With your custom domain now pointing to your GitLab Pages, your projects stand not just on the vast internet, but on a piece of it that is uniquely yours.

Remember, the path to mastering the digital realm is filled with learning and exploration. Today, you've taken a significant step on that journey, making your mark in the online universe.

